import { useEffect, useState } from "react";
import axios from "axios";
import HomeWithFooter from "../Layout/HomeWithFooter";
import Cart from "../components/cart";
import { sumBy } from "lodash";
function CartPage(props) {
  const { cart } = props;
  const [total, setTotal] = useState();
  useEffect(() => {
    setTotal(
      sumBy(cart, function (o) {
        return o.totalPrice;
      })
    );
  }, [cart]);
  return (
    <HomeWithFooter cart={total}>
      <Cart cartItem={cart} setTotal={setTotal} />
    </HomeWithFooter>
  );
}
export async function getServerSideProps({ req, locale }) {
  try {
    let cart = await axios.get("http://localhost:3001/getCart");

    return {
      props: {
        cart: cart.data,
      },
    };
  } catch (error) {
    return {
      props: {
        errorCode: 404,
        cart: {},
      },
    };
  }
}
export default CartPage;
