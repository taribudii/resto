import { Fragment, useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Paper from "@mui/material/Paper";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import FilledInput from "@mui/material/FilledInput";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import axios from "axios";
import { useRouter } from "next/router";

export default function OutlinedCard() {
  const [showPassword, setShowPassword] = useState(false);
  const router = useRouter();
  let initialData = {
    username: "",
    password: "",
  };
  useEffect(() => {
    document.cookie.split("isLogin=")[1] !== "false"
      ? (router.push = "/menu")
      : "";
  }, []);
  const [data, setData] = useState(initialData);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleChange = (name, value) => {
    const newData = {
      ...data,
      [name]: value,
    };
    setData(newData);
  };
  async function login() {
    let ress = await axios.post("http://localhost:3001/signin", data);
    if (ress.status === 200) {
      router.push = `/menu`;
      document.cookie = "isLogin=true";
    }
  }
  return (
    <Box
      sx={{
        backgroundColor: "black",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          width: 500,
          padding: "10px",
          backgroundColor: "black",
        }}
      >
        <Paper
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CardContent sx={{ justifyContent: "center", mt: 4 }}>
            <Typography variant="h5" textAlign="center">
              Hi Admin!
            </Typography>
            <FormControl
              sx={{ width: "100%", borderRadius: "30%", mb: 3, mt: 4 }}
              variant="filled"
            >
              <InputLabel
                htmlFor="filled-adornment-username"
                style={{ color: "black" }}
              >
                Username
              </InputLabel>
              <FilledInput
                id="filled-adornment-username"
                type="text"
                name="username"
                onChange={(e) => handleChange(e.target.name, e.target.value)}
              />
            </FormControl>
            <FormControl
              sx={{ width: "100%", borderRadius: "30%" }}
              variant="filled"
            >
              <InputLabel
                htmlFor="filled-adornment-password"
                style={{ color: "black" }}
              >
                Password
              </InputLabel>
              <FilledInput
                id="filled-adornment-password"
                type={showPassword ? "text" : "password"}
                name="password"
                onChange={(e) => handleChange(e.target.name, e.target.value)}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </CardContent>
          <CardActions sx={{ mb: 4 }}>
            <Button size="medium" variant="contained" onClick={() => login()}>
              Login
            </Button>
          </CardActions>
        </Paper>
      </Box>
    </Box>
  );
}
