import React, { useState, MouseEvent } from "react";
import Home from "../Layout/Home";
import ListMenu from "../components/menus/listMenu";
function MenuPage() {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <Home>
      <ListMenu />
    </Home>
  );
}
export default MenuPage;
