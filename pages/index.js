import React, { useEffect, useState, MouseEvent } from "react";
import axios from "axios";
import Home from "../Layout/Home";
import Grid from "@mui/material/Grid";
import MenuItem from "../components/menus/menuItem";
import { useNavigate } from "react-router-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import Typography from "@mui/material/Typography";

function IndexPage() {
  const [cart, setCart] = useState([]);
  const [signature, setSignature] = useState([]);
  const [banners, setBanners] = useState([]);

  const getData = async () => {
    let menus = await axios.get("http://localhost:3001/getMenus");
    setSignature(menus.data.filter((a) => a.type === "food").slice(0, 4));
  };
  const getBanner = async () => {
    let banner = await axios.get("http://localhost:3001/getBanners");
    setBanners(banner.data);
  };

  const putCart = async (payload) => {
    let carts = await axios.put("http://localhost:3001/addToCart", payload);
  };

  const toMenu = () => {
    location.href = "/menu";
  };

  const addToCart = (newItem) => {
    setCart((cart) => {
      return [...cart, newItem];
    });
    putCart(newItem);
  };

  useEffect(() => {
    getData();
    getBanner();
  }, []);

  return (
    <Home>
      <div className="home-container">
        <div>
          <Carousel autoPlay={true} showArrows={false} showThumbs={false}>
            {banners.map((item) => (
              <div className="item-carousel" key={item.url} onClick={toMenu}>
                <img src={item.url} />
              </div>
            ))}
          </Carousel>
        </div>
        <Typography variant="h4" sx={{ color: "black", mt: 5, mb: 3 }}>
          Signature Menu
        </Typography>
        <Grid container spacing={2}>
          {signature.map((item) => (
            <Grid item xs={12} md={3} key={item.id}>
              {/* <Item> */}
              <MenuItem item={item} addToCart={addToCart} />
              {/* </Item> */}
            </Grid>
          ))}
        </Grid>
      </div>
    </Home>
  );
}

export default IndexPage;
