const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { elementType } = require("prop-types");
const app = express();
const port = 3001;
const corsOptions = {
  origin: "*",
  credentials: true,
  optionSuccessStatus: 200,
};
app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const menus = [
  {
    id: 1,
    name: "Chiken Burger",
    type: "food",
    price: "40.000",
    description:
      "Burger special dengan chiken crispy, dilengkapi dengan kentang goreng dan saus pedas andalan.",
    image:
      "https://images.unsplash.com/photo-1551782450-17144efb9c50?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=869&q=80",
  },
  {
    id: 2,
    name: "Cheese Beef Burger",
    type: "food",
    price: "55.000",
    description:
      "Burger dengan beef olahan yang di olah dengan rempah-rempah alami dan menawarkan sensasi rasa tersendiri",
    image:
      "https://images.unsplash.com/photo-1572802419224-296b0aeee0d9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=815&q=80",
  },
  {
    id: 3,
    name: "Thunderloin Beef Steak",
    type: "food",
    price: "200.000",
    description:
      "Thunderloin beef steak menggunakan thunderloin pilihan, anda juga dapat memilih sauce sesuai selera anda.",
    image:
      "https://images.unsplash.com/photo-1551782450-17144efb9c50?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=869&q=80",
  },
  {
    id: 4,
    name: "Italian Pasta",
    type: "food",
    price: "75.000",
    description:
      "Dimasak dengan bumbu dan cara italia asli yang dapat menawarkan rasa yang unik",
    image:
      "https://images.unsplash.com/photo-1579684947550-22e945225d9a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80",
  },
  {
    id: 5,
    name: "Salmon Sushi",
    type: "food",
    price: "150.000",
    description:
      "Menggunakan salmon fresh yang membuat lidah anda lebih nikmat.",
    image:
      "https://images.unsplash.com/photo-1617196905100-216ffe128142?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=864&q=80",
  },
  {
    id: 6,
    name: "Ice Tea",
    type: "drink",
    price: "15.000",
    description:
      "Dapat menjadi minuman yang tepat untuk menyegarkan tubuh anda.",
    image:
      "https://images.unsplash.com/photo-1595252046564-55f55a02afad?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80",
  },
  {
    id: 7,
    name: "Lemon Juice",
    type: "drink",
    price: "15.000",
    description:
      "Dapat menjadi minuman yang tepat untuk menyegarkan tubuh anda.",
    image:
      "https://images.unsplash.com/photo-1621263764928-df1444c5e859?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80",
  },
  {
    id: 8,
    name: "Coffe Latte",
    type: "drink",
    price: "25.000",
    description:
      "Menggunakan kopi lokal pilihan, dengan rasa signature yang dapat memanjakan anda",
    image:
      "https://images.unsplash.com/photo-1622240506921-042a4e71c172?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80",
  },
  {
    id: 9,
    name: "Mango Juice",
    type: "drink",
    price: "20.000",
    description:
      "Dapat menjadi minuman yang tepat untuk menyegarkan tubuh anda.",
    image:
      "https://images.unsplash.com/photo-1604298331663-de303fbc7059?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80",
  },
];
const banners = [
  {
    id: 1,
    url: "https://i.ytimg.com/vi/sz59T2tatAY/maxresdefault.jpg",
  },
  {
    id: 2,
    url: "https://images.unsplash.com/photo-1659088981151-b5ce611010b0?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
  },
  {
    id: 3,
    url: "https://images.unsplash.com/photo-1561758033-d89a9ad46330?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
  },
];
const cart = [];

app.get("/getMenus", (req, res) => {
  res.send(menus);
});

app.get("/getBanners", (req, res) => {
  res.send(banners);
});

app.get("/getCart", (req, res) => {
  let cartItem = cart.reduce((ar, obj) => {
    let bool = false;
    if (!ar) {
      ar = [];
    }
    ar.forEach((a) => {
      if (a.id === obj.id) {
        a.count++;
        bool = true;
      }
    });
    if (!bool) {
      obj.count = 1;
      ar.push(obj);
    }
    return ar;
  }, []);

  cartItem.forEach((element) => {
    Object.assign(element, { totalPrice: element.price * element.count });
  });
  res.send(cartItem);
});

app.put("/addToCart", (req, res) => {
  if (
    req.body.id !== undefined &&
    req.body.name !== undefined &&
    req.body.price !== undefined
  ) {
    let item = {
      id: req.body.id,
      name: req.body.name,
      price: req.body.price,
      image: req.body.image,
    };
    cart.push(item);
    res.status = 202;
    res.send("Added to Cart");
  } else {
    res.status = 400;
    res.send("Fail added");
  }
});

app.put("/deleteCartItem", (req, res) => {
  if (
    req.body.id !== undefined &&
    req.body.name !== undefined &&
    req.body.price !== undefined
  ) {
    let item = {
      id: req.body.id,
      name: req.body.name,
      price: req.body.price,
      image: req.body.image,
    };
    for (var i = 0; i < cart.length; i++) {
      if (cart[i].id == item.id) {
        cart.splice(i, 1);
        break;
      }
    }
    res.status = 202;
    res.send(cart);
  } else {
    res.status = 400;
    res.send("Fail delete");
  }
});

app.put("/deleteMenuItem", (req, res) => {
  if (
    req.body.id !== undefined &&
    req.body.name !== undefined &&
    req.body.price !== undefined
  ) {
    let item = {
      id: req.body.id,
      name: req.body.name,
      price: req.body.price,
      image: req.body.image,
    };
    for (var i = 0; i < menus.length; i++) {
      if (menus[i].id == item.id) {
        menus.splice(i, 1);
        break;
      }
    }
    res.status = 202;
    res.send(menus);
  } else {
    res.status = 400;
    res.send("Fail delete");
  }
});

app.put("/addMenus", (req, res) => {
  if (
    req.body.id !== undefined &&
    req.body.name !== undefined &&
    req.body.price !== undefined &&
    req.body.type !== undefined &&
    req.body.description !== undefined
  ) {
    let item = {
      id: req.body.id,
      name: req.body.name,
      price: req.body.price,
      type: req.body.type,
      description: req.body.description,
    };
    menus.push(item);
    res.status = 202;
    res.send("Menu Successfully added");
  } else {
    res.status = 400;
    res.send("Fail added");
  }
});

app.post("/signin", (req, res) => {
  if (req.body.username !== undefined && req.body.password !== undefined) {
    if (req.body.username === "admin" && req.body.password === "admin") {
      res.status = 200;
      res.send("Login Successfull");
    }
  } else {
    res.status = 400;
    res.send("Login Fail");
  }
});

app.listen(port, () => {
  console.log(cart);
});
