import axios from "axios";

async function getData() {
  const res = await axios.get("/getMenus");
  const data = res.data;
  return data;
  // do something with the data
}

export { getData };
