import React, { useState, useEffect } from "react";
// import { bindActionCreators } from "redux";
import Router from "next/router";
import { useTheme } from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";

import Box from "@mui/material/Box";

import AppBar from "../components/layout/appBar";
import Fade from "@mui/material/Fade";
import Footer from "../components/layout/footer";

const useStyles = makeStyles((theme) => {
  return {};
});

function Home(props) {
  const { cart, children } = props;

  const theme = useTheme();

  useEffect(() => {
    cart;
  }, [cart]);

  const classes = useStyles();

  return (
    <Box>
      <AppBar />
      {children}
      <Footer totalPrice={cart} />
    </Box>
  );
}

export default Home;
