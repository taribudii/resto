import React, { useState, useEffect } from "react";
// import { bindActionCreators } from "redux";
import Router from "next/router";
import { useTheme } from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";

import Box from "@mui/material/Box";

import AppBar from "../components/layout/appBar";
import Fade from "@mui/material/Fade";
import axios from "axios";
// import clsx from "clsx";

const useStyles = makeStyles((theme) => {
  return {};
});



function Home(props) {
  const { children } = props;
  const theme = useTheme();
  const [menu, setMenu] = React.useState(null);

  useEffect(() => {
    // axios.get('http://localhost:3001/getMenus').then((response) => {
    //   setMenu(response);
    //   console.error(response)
    // });
  },[]);

  const classes = useStyles();

  return (
    <div>
      <Box>
        <AppBar />
        {children}
      </Box>
    </div>
  );
}

export default Home;
