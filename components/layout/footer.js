import React, { useState, MouseEvent } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket";

function Footer(props) {
  const { totalPrice } = props;
  return (
    <div
      style={{
        position: "fixed",
        left: 0,
        bottom: 0,
        width: "100%",
        backgroundColor: "black",
        color: "white",
        textAlign: "center",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-between",
          m: "10px 25px 20px 25px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
          }}
        >
          {" "}
          <ShoppingBasketIcon
            sx={{
              mr: 10,
            }}
          />
          <Typography variant="h6" style={{ fontWeight: "inherit" }}>
            Rp. {totalPrice}.000
          </Typography>
        </Box>
        <Box>
          <Button variant="contained" color="primary">
            Checkout
          </Button>
        </Box>
      </Box>
    </div>
  );
}
export default Footer;
