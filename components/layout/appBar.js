import React, { useState, MouseEvent, useEffect } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import FastfoodIcon from "@mui/icons-material/Fastfood";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Image from "next/image";
import axios from "axios";
import { useRouter } from "next/router";

function Appbar() {
  let initialData = {
    id: "",
    name: "",
    type: "",
    price: "",
    description: "",
  };
  const router = useRouter();
  const [isLogin, setIsLogin] = useState();
  const [open, setOpen] = useState(false);
  const [data, setData] = useState(initialData);
  const [type, setType] = useState("");
  useEffect(() => {
    setIsLogin(document.cookie.split("isLogin=")[1]);
  }, []);

  const toMenu = () => {
    location.href = "/menu";
  };

  const toHome = () => {
    location.href = "/index";
  };

  const toCart = () => {
    location.href = "/cart";
  };

  const addMenu = () => {
    setOpen(true);
  };

  const handleChange = (event) => {
    setType(event.target.value);
  };

  const handleChangeForm = (name, value) => {
    const newData = {
      ...data,
      [name]: value,
    };
    setData(newData);
  };
  const addMenus = async () => {
    data.id = Math.floor(Math.random() * 100) + 1;
    data.type = type;
    data.image =
      "https://images.unsplash.com/photo-1579684947550-22e945225d9a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80";
    let ress = await axios.put("http://localhost:3001/addMenus", data);

    if (ress.status === 200) {
      setOpen(false);
      router.push("/menu");
    }
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <AppBar
      position="fixed"
      sx={
        isLogin === "true"
          ? { backgroundColor: "red" }
          : { backgroundColor: "primary" }
      }
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters className="tool-bar">
          <div className="logo-app" onClick={toCart}>
            <FastfoodIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component="a"
              href="/"
              sx={{
                mr: 2,
                display: { xs: "none", md: "flex" },
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
              }}
            >
              CBevrages
            </Typography>
          </div>
          {isLogin !== "true" ? (
            <div className="menu-bar">
              <div className="menu" onClick={toMenu}>
                Menu
              </div>
              <Image
                className="cart-icon"
                src="/cart.png"
                width={20}
                height={20}
                onClick={toCart}
              />
            </div>
          ) : (
            <div className="menu-bar">
              <div className="menu" onClick={addMenu}>
                Add Menu
              </div>
              <div
                className="menu"
                onClick={() => {
                  window.location.href = `/admin`;
                  document.cookie = "isLogin=false";
                }}
              >
                Logout
              </div>
              <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Add Menu Item</DialogTitle>
                <DialogContent>
                  <TextField
                    autoFocus
                    margin="dense"
                    sx={{ mb: 2 }}
                    name="name"
                    label="Menu's Name"
                    type="text"
                    fullWidth
                    variant="outlined"
                    onChange={(e) =>
                      handleChangeForm(e.target.name, e.target.value)
                    }
                  />
                  <TextField
                    autoFocus
                    margin="dense"
                    sx={{ mb: 2 }}
                    name="price"
                    label="Menu's Price"
                    type="text"
                    fullWidth
                    variant="outlined"
                    onChange={(e) =>
                      handleChangeForm(e.target.name, e.target.value)
                    }
                  />
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Type</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={type}
                      label="Type"
                      onChange={handleChange}
                    >
                      <MenuItem value="food">Food</MenuItem>
                      <MenuItem value="drink">Drink</MenuItem>
                    </Select>
                  </FormControl>
                  <TextField
                    autoFocus
                    margin="dense"
                    sx={{ mb: 2 }}
                    name="description"
                    label="Menu's Description"
                    type="text"
                    fullWidth
                    variant="outlined"
                    onChange={(e) =>
                      handleChangeForm(e.target.name, e.target.value)
                    }
                  />
                </DialogContent>
                <DialogActions sx={{ mr: 2, mb: 2 }}>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={handleClose}
                  >
                    Cancel
                  </Button>
                  <Button variant="contained" onClick={addMenus}>
                    Add Menu
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          )}
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default Appbar;
