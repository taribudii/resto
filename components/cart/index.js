import React, { useEffect, useState } from "react";
import axios from "axios";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import { sumBy } from "lodash";
import { useRouter } from "next/router";

export default function CartIndex(props) {
  const [cartItem, setCartItem] = useState(props.cartItem);
  const router = useRouter();
  const deleteItem = async (payload) => {
    const ress = await axios.put(
      "http://localhost:3001/deleteCartItem",
      payload
    );
    setCartItem(ress.data);
    let totalPrice = sumBy(cartItem, function (o) {
      return o.totalPrice;
    });
    props.setTotal(totalPrice);
    router.push("/cart");
  };
  return (
    <Box
      sx={{
        mt: 5,
        pt: 5,
        mb: 10,
      }}
    >
      {cartItem.map((i) => (
        <Paper key={i.id} sx={{ mb: 2, p: 3 }}>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              alignItems: "center",
            }}
          >
            <div className="cart-item">
              <Box sx={{ mr: 3 }}>
                <img
                  src={i.image}
                  style={{
                    width: "200px",
                    height: "200px",
                    objectFit: "cover",
                  }}
                />
              </Box>
              <Box className="trash">
                <Image
                  className="trash-icon"
                  src="/trash.png"
                  width={20}
                  height={20}
                  onClick={() => deleteItem(i)}
                />
              </Box>
            </div>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
                alignItems: "center",
              }}
            >
              <Box>
                <Typography variant="h6" style={{ fontWeight: "bold" }}>
                  {i.name}
                </Typography>
                <Typography>Rp. {i.price}</Typography>
                <Typography color="#9e9e9e">x {i.count}</Typography>
              </Box>
              <Box>
                <Typography
                  className="currency"
                  variant="h6"
                  style={{ fontWeight: "bold" }}
                >
                  {i.totalPrice}.000
                </Typography>
              </Box>
            </Box>
          </Box>
        </Paper>
      ))}
    </Box>
  );
}
