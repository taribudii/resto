import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Backdrop from "@mui/material/Backdrop";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
export default function MediaCard(props) {
  const { item, addToCart, deleteMenu } = props;
  const [isLogin, setIsLogin] = useState();
  const [open, setOpen] = useState(false);
  useEffect(() => {
    setIsLogin(document.cookie.split("isLogin=")[1]);
  }, []);
  return (
    <Card sx={{ maxWidth: 345 }} className="pilihan-menu">
      <CardMedia sx={{ height: 240 }} image={item.image} />
      <CardContent sx={{ height: 130 }}>
        <Typography gutterBottom variant="h5" component="div">
          {item.name}
        </Typography>
        <Typography variant="body4">Rp.{item.price}</Typography>
        <Typography variant="body2" color="text.secondary">
          {item.description}
        </Typography>
      </CardContent>
      <CardActions
        spacing={2}
        sx={{ display: "flex", justifyContent: "end", mb: 1 }}
      >
        {isLogin === "true" ? (
          <Button
            size="small"
            variant="contained"
            color="error"
            onClick={() => setOpen(true)}
          >
            Delete Menu
          </Button>
        ) : (
          <Button
            size="small"
            variant="contained"
            onClick={() => addToCart(item)}
          >
            Add to Cart
          </Button>
        )}
        <Dialog open={open} onClose={() => setOpen(false)}>
          <DialogTitle>Warning!</DialogTitle>
          <DialogContent>
            <Typography gutterBottom variant="h5" component="div">
              Are you sure want to delete {item.name}?
            </Typography>
          </DialogContent>
          <DialogActions sx={{ mr: 2, mb: 2 }}>
            <Button
              variant="contained"
              color="error"
              onClick={() => setOpen(false)}
            >
              Cancel
            </Button>
            <Button variant="contained" onClick={() => deleteMenu(item)}>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </CardActions>
    </Card>
  );
}
