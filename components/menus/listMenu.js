import { useEffect, useState } from "react";
import axios from "axios";
import { styled } from "@mui/material/styles";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import MenuItem from "./menuItem";
import { getData } from "../../api/services";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function FullWidthGrid(props) {
  const [cart, setCart] = useState([]);
  const [menu, setMenu] = useState([]);
  const [food, setFood] = useState([]);
  const [drink, setDrink] = useState([]);
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const getData = async () => {
    let menus = await axios.get("http://localhost:3001/getMenus");
    setMenu(menus.data);
    setFood(menus.data.filter((a) => a.type === "food"));
    setDrink(menus.data.filter((a) => a.type === "drink"));
  };

  const getCart = async () => {
    let cart = await axios.get("http://localhost:3001/getCart");
  };

  const putCart = async (payload) => {
    let carts = await axios.put("http://localhost:3001/addToCart", payload);
  };

  const deleteMenu = async (payload) => {
    let ress = await axios.put("http://localhost:3001/deleteMenuItem", payload);
    setMenu(ress.data);
  };

  useEffect(() => {
    getData();
    getCart();
  }, [cart]);
  const addToCart = (newItem) => {
    setCart((cart) => {
      return [...cart, newItem];
    });
    putCart(newItem);
  };
  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
      </div>
    );
  }

  return (
    <Box sx={{ flexGrow: 1, padding: 3, marginTop: 7, height: "92vh" }}>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="basic tabs example"
        sx={{
          mb: 3,
          color: "white",
        }}
      >
        <Tab label="All Menu" {...a11yProps(0)} />
        <Tab label="Makanan" {...a11yProps(1)} />
        <Tab label="Minuman" {...a11yProps(2)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <Grid container spacing={2}>
          {menu.map((item) => (
            <Grid item xs={12} md={3} key={item.id}>
              <MenuItem
                item={item}
                addToCart={addToCart}
                deleteMenu={deleteMenu}
              />
            </Grid>
          ))}
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Grid container spacing={2}>
          {food.map((item) => (
            <Grid item xs={12} md={3} key={item.id}>
              <MenuItem
                item={item}
                addToCart={addToCart}
                deleteMenu={deleteMenu}
              />
            </Grid>
          ))}
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Grid container spacing={2}>
          {drink.map((item) => (
            <Grid item xs={12} md={3} key={item.id}>
              <MenuItem
                item={item}
                addToCart={addToCart}
                deleteMenu={deleteMenu}
              />
            </Grid>
          ))}
        </Grid>
      </TabPanel>
    </Box>
  );
}
