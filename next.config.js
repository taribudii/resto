/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
};

module.exports = nextConfig;
// next.config.js
// module.exports = {
//   server: {
//     port: 3001, // the server will run on port 3000
//     host: "0.0.0.0", // the server will be accessible at localhost:3000 or the IP of your machine
//   },
// };
